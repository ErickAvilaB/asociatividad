"""
Programa que muestra la asociatividad de la suma de vectores: (X + Y) + Z = X + (Y + Z)
autor: Avila Barba Erick Yahir
date: Febrero 7, 2024
"""


from numpy import array  # Soporte para vectores
import matplotlib.pyplot as plt  # Soporte para gráficos


# Define vectores
origen = array([0, 0])
x = array([3, 2])
y = array([1, 3])
z = array([4, 1])

# Calcula la suma de vectores X + Y + Z
xyz = x + y + z
# Calcula la suma de vectores (X + Y) + Z
xy = x + y
xyz1 = xy + z
# Calcula la suma de vectores X + (Y + Z)
yz = y + z
xyz2 = x + yz


# Crea la configuración de cada flecha
arrows = [
    (origen, xyz, 'b'),  # X + Y + Z
    (origen, xy, 'g'),
    (xy, z, 'g'),        # (X + Y) + Z
    (origen, yz, 'r'),
    (yz, x, 'r')         # X + (Y + Z)
]

#                 PLOT
# p initial vector (point)
# v end vector
# c color

for p, v, c in arrows:
    plt.quiver(p[0], p[1], v[0], v[1],
               color=c, units='xy', scale=1)

# Configuración del gráfico
plt.axis('scaled'), plt.xlim(0, 10), plt.ylim(0, 10)

# Muestra los puntos de las sumas
plt.scatter(*xyz1, color='g', label='(X + Y) + Z')
plt.scatter(*xyz2, color='r', label='X + (Y + Z)')
plt.scatter(*xyz, color='b', label='X + Y + Z')

# Crea la simbología
plt.legend()

# Muestra el gráfico
plt.show()
